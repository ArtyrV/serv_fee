#pragma once
#include <QCoreApplication>
#include <QtNetwork/QNetworkInterface>
#include <iostream>
#include <QObject>
#include <QtNetwork/QTcpSocket>
#include<QtNetwork/QTcpServer>
#include <QDebug>
#include<QtSql/QSqlDatabase>
#include<QtSql/QSqlQuery>
#include<QSqlRecord>
#include <QSqlError>
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include<QJsonValue>
#include<QJsonDocument>
#include<QFile>
#include<QTime>
#include<QDate>

class myHTTPserver : public QObject
{
    Q_OBJECT
public:
    char    *method,    // "GET" or "POST"
            *uri,       // "/index.html" things before '?'
            *qs,        // "a=1&b=2"     things after  '?'
            *prot;      // "HTTP/1.1"
    explicit myHTTPserver(QObject *parent = 0);
    ~myHTTPserver();
    QTcpSocket *socket ;
public slots:
    void myConnection();
private:
    qint64 bytesAvailable() const;
    QTcpServer *server;
    void db(QMap<QString,QString>* q);
    QMap<QString, QString>* z;
    QMap<QString, QString>* parse (char*);
QJsonObject *jo;

};
