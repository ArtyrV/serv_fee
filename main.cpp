
#include "zap.h"
using namespace std;

void delayms( int millisecondsToWait );

void messageToFile(QtMsgType type,
                   const QMessageLogContext& context,
                   const QString &msg)
{
  QFile file("fee_inf.log");
  if(!file.open(QIODevice::WriteOnly|QIODevice::Text|QIODevice::Append)) return;
 QTime currTime = QTime::currentTime();
          QTextStream out(&file);
QDate date=QDate::currentDate();
          switch(type)
          {
            case QtDebugMsg:
              out<< date.toString("dd/MM/yy")<<"|"<<currTime.toString("hh:mm:ss")<<"| Debug: "<<msg<<", "<<context.file<<endl;
              break;
          case QtWarningMsg:
            out<< date.toString("dd/MM/yy")<<"|"<<currTime.toString("hh:mm:ss")<<"| Warning: "<<msg<<", "<<context.file<<endl;
            break;
          case QtCriticalMsg:
            out<< date.toString("dd/MM/yy")<<"|"<<currTime.toString("hh:mm:ss")<<"| Critical: "<<msg<<", "<<context.file<<endl;
            break;
          case QtFatalMsg:
            out<< date.toString("dd/MM/yy")<<"|"<<currTime.toString("hh:mm:ss")<<"| Fatal: "<<msg<<", "<<context.file<<endl;
            abort();
          }
}


int main(int argc, char *argv[])
    {
    QCoreApplication a(argc, argv);
    qInstallMessageHandler(messageToFile);
    myHTTPserver server;
    return a.exec();
}

myHTTPserver::myHTTPserver(QObject *parent) : QObject(parent)
    {
    qDebug()<<"----------------------------------------------------------------------------------------------------";

    QString val;
    QFile conf;

   // if(!conf->exists())

    conf.setFileName("config.json");
    conf.open(QIODevice::ReadOnly | QIODevice::Text);
    val=conf.readAll();
    conf.close();

    qWarning() << val.toUtf8();


    // Cоздаём объект Json Document, считав в него все данные из ответа
    QJsonDocument jd = QJsonDocument::fromJson(val.toUtf8());

    // Забираем из документа корневой объект
    jo=new QJsonObject();
    *jo=jd.object();

  //  qDebug()<<jo->keys().at(1)<<' '<<jo->value(jo->keys().at(1)).toString();

    QJsonValue n= jo->value("fee_port");

  //  qDebug()<<jo->value("fee_port");
     server = new QTcpServer(this);
    // waiting for the web brower to make contact,this will emit signal
    connect(server, SIGNAL(newConnection()),this, SLOT(myConnection()));
    if(!server->listen(QHostAddress::Any, n.toInt()))
    {qDebug()<<server->errorString();
    qDebug()<<"У тебя сервак не стоит!";
    }

   // else cout<<"\nWeb server is waiting for a connection on port 8080";
}

void myHTTPserver::myConnection()
    {
    static qint16 count;  //count number to be displayed on web browser
    socket = server->nextPendingConnection();
    while(!(socket->waitForReadyRead(100)));  //waiting for data to be read from web browser

    char webBrowerRXData[1000];
    int sv=socket->read(webBrowerRXData,1000);
   // cout<<"\nreading web browser data=\n";
    //for(int i=0;i<sv;i++)cout<<webBrowerRXData[i];
                method = strtok(webBrowerRXData,  " \t\r\n");
                uri    = strtok(NULL, " \t");
                prot   = strtok(NULL, " \t\r\n");
                if (qs = strchr(uri, '?'))
                       {
                           *qs++ = '\0'; //split URI
                           z=parse(qs);

                       }




    socket->flush();
    connect(socket, SIGNAL(disconnected()),socket, SLOT(deleteLater()));
    socket->disconnectFromHost();

}

myHTTPserver::~myHTTPserver()
    {
    socket->close();
}

void myHTTPserver::db(QMap<QString,QString>*q)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
            db.setDatabaseName(jo->value("db_name").toString());
            db.setUserName(jo->value("db_user_name").toString());
            db.setHostName(jo->value("db_host_name").toString());
            db.setPassword(jo->value("db_pass").toString());
            db.setPort(jo->value("fee_port").toInt());
if(!db.open()) qDebug()<<db.lastError().text();
else
{
    qDebug()<<jo->value("db_name")<<jo->value("db_user_name")<<jo->value("db_host_name")<<jo->value("db_pass")<<jo->value("fee_port");
 QString qw;
 QSqlQuery *query=new QSqlQuery(db);
 QJsonDocument  json;
 QJsonArray     recordsArray;

 for(int i=0;i<100;i++)
{


    if((*q)["method"]=="fee_user_cur")
    {
        int x= (*q)["user_id"].toInt()%100;
        qw+="SELECT user_id, market, SUM(deal_fee)/2 as fee FROM user_deal_history_"+QString::number(x);
        qw+=" WHERE market LIKE '%"+(*q)["cur"]+"%' AND user_id="+(*q)["user_id"]+" AND time BETWEEN "+(*q)["ltime"]+" AND "+(*q)["htime"];
        qw+=" GROUP BY user_id, market;";

        query->exec(qw);
        qw.clear();
       while(query->next())
        {
           QJsonObject recordObject;

              for(int x=0; x < query->record().count(); x++)
              {
              recordObject.insert( query->record().fieldName(x), QJsonValue::fromVariant(query->value(x)) );
              }
           recordsArray.push_back(recordObject);
        }
       break;
    }
      else if((*q)["method"]=="fee_users_cur")
    {
        qw+="SELECT user_id, market, SUM(deal_fee)/2 as fee FROM user_deal_history_"+QString::number(i);
        qw+=" WHERE market LIKE '%"+(*q)["cur"]+"%' AND time BETWEEN "+(*q)["ltime"]+" AND "+(*q)["htime"];
qw+=" GROUP BY user_id, market;";
    }
  else  if((*q)["method"]=="fee_user_curs")
    {
        qw+="SELECT user_id, market, SUM(deal_fee)/2 as fee FROM user_deal_history_"+QString::number(i);
        qw+=" WHERE user_id="+(*q)["user_id"]+" AND time BETWEEN "+(*q)["ltime"]+" AND "+(*q)["htime"];
qw+=" GROUP BY user_id, market;";
    }
   else if((*q)["method"]=="fee_users_curs")
    {
        qw+="SELECT user_id, market, SUM(deal_fee)/2 as fee FROM user_deal_history_"+QString::number(i);
        qw+=" WHERE time BETWEEN "+(*q)["ltime"]+" AND "+(*q)["htime"];
qw+=" GROUP BY user_id, market;";
    }




    query->exec(qw);
    qDebug()<<query->lastError();
    qw.clear();


           while(query->next())
            {
               QJsonObject recordObject;

                  for(int x=0; x < query->record().count(); x++)
                  {
                  recordObject.insert( query->record().fieldName(x), QJsonValue::fromVariant(query->value(x)) );
                  }
               recordsArray.push_back(recordObject);
            }


            //socket->write(query->value(t).toByteArray());
   //  socket->write();
   //qDebug()<<i;
  }

 json.setArray(recordsArray);

socket->write(json.toJson());
db.close();

}

// qDebug()<<query->lastQuery();
 //qDebug()<<query->lastError();

}



QMap<QString,QString>* myHTTPserver::parse(char *qs)
{
    char*x, *y;
    QMap <QString,QString> map;
        x=strtok(qs, "=");
        y=strtok(NULL, "&");
        map[x]=y;
        qDebug()<<x<<" : "<<y;
    while(y!=NULL)
    {
        x=strtok(NULL, "=");
        y=strtok(NULL, "&");
        map[x]=y;
         qDebug()<<x<<" : "<<y;
    }
    db(&map);

return &map;
}

